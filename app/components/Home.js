import React, { Component } from 'react';
import Search from './Search';
import PostsList from './PostsList';
import Data from '../data';
import Navigation from './Navigation';
require('./home.scss');
export class Home extends Component {
    render() {
        return (
            <div className="container">
            	<h1>Welcome to Coupler</h1>
            	<Navigation />
        		<Search />
        		<PostsList posts={Data} />
        		<footer>
        		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        		</footer>
            </div>
        );
    }
}
export default Home;
