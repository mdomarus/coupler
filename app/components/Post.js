import React, { Component } from 'react';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
require('./post.scss');
export class Post extends Component {
    render() {
        return (
            <div className={'post post-id--' + this.props.post.id}>
                
                <PostImage image={this.props.post.image || this.props.post.avatar} alt={this.props.post.names || this.props.post.fullName} />
                <PostNames names={this.props.post.names || this.props.post.fullName } />
                <PostDate dateFrom={this.props.post.dateFrom} dateTo={this.props.post.dateTo} />
                <PostLocation location={this.props.post.location} />
            </div>
        );
    }
}
let PostNames = createReactClass({
    render: function() {
        return (
            <div className="post__names">Who: {this.props.names}</div>
        );
    },
});
let PostImage = createReactClass({
    render: function() {
        let image = this.props.image,
            alt = this.props.alt;
        return (
            <img className="post__image" src={image} alt={alt} />
        );
    }
});
let PostLocation = createReactClass({
    render: function() {
        return (
            <div className="post__location">Where: {this.props.location}</div>
        );
    }
});
let PostDate = createReactClass({
    render: function() {
        return (
            <div className="post__date">When: {this.props.dateFrom} - {this.props.dateTo}</div>);
    }
})
export default Post;
