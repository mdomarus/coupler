import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Post } from './Post';
require('./postslist.scss');

class PostsList extends Component {
    render() {
        let posts = this.props.posts.map(function(item, key) {
            return (
                <Post post={item} key={key} />
            );
        }),
            postsHeader = "Latest posts";
        return (
            <div className="posts">
                <div className="posts-header">{postsHeader}</div>
                {posts}
            </div>
        );
    }
}
PostsList.propTypes = {
    posts: PropTypes.array.isRequired
};
export default PostsList;
