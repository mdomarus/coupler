import React, { Component } from 'react';
import createReactClass from 'create-react-class';
import PropTypes from 'prop-types';
require('./search.scss');
export class Search extends Component {
    render() {
        return (
            <div className={'search'}>

                <h2>Search</h2>
                <div className="form-group">
                	<label htmlFor="date-from">From</label>
                	<input name="date-from" type="date" className="form-control" id="date-from" />
                </div>
                <div className="form-group">
                	<label htmlFor="date-to">To</label>
                	<input name="date-to" type="date" className="form-control" id="date-to" />
                </div>
                <div className="form-group">
                	<label htmlFor="location">Location</label>
                	<input name="location" type="text" className="form-control" id="location" />
                </div>
            </div>
        );
    }
}
export default Search;
