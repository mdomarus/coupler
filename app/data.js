var faker = require('faker');
var chance = require('chance').Chance();

const Data = [];

for (let x = 0; x < 15; x++) {
    var profile = {};
    profile.id = chance.guid();
    profile.image = faker.image.avatar();
    profile.dateFrom = chance.date({string: true, american: false});
    profile.dateTo = chance.date({string: true, american: false});
    profile.names = faker.name.firstName(0) + ' & ' + faker.name.firstName(1);
    profile.location = faker.address.country() + ' / ' + faker.address.city();
    Data.push(profile);
}

export default Data;
