City: ''
Country: ''
Availability: always, since ... , until ..., since - until
Email: 
ID: []
Image: path
Names: ''
Region: ''

<posts-list>
	<post>
		<post-names></post-names>
		<post-image></post-image>
		<post-location>
			<post-country></post-country>
			<post-region></post-region>
			<post-city></post-city>
		</post-location>
		<post-availability></post-availability>
	</post>
</posts-list>